import React from "react";
import { StyleSheet, Text, View } from "react-native";

class Texto extends React.Component {
  state = {
    texto: "Hola mundo"
  };

  handlePress = () => {
    this.setState({ texto: "Adios Mundo Cruel" });
  };

  render() {
    const { texto } = this.state;
    return <Text onPress={this.handlePress}>{texto}</Text>;
  }
}

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Texto de prueba</Text>
      <Texto />
      <Text style={styles.text2}>Texto de prueba</Text>
      <Text style={styles.text3}>Texto de prueba</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    flex: 1,
    height: 100,
    width: 100,
    backgroundColor: "brown",
    color: "blue"
  },
  text2: {
    flex: 2,
    height: 100,
    width: 100,
    backgroundColor: "yellow",
    color: "blue"
  },
  text3: {
    flex: 3,
    height: 100,
    width: 100,
    backgroundColor: 'orange',
    color: 'blue',
   },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
